FROM ubuntu:18.04
RUN apt-get update
RUN apt-get install -y curl python3-pip
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -
RUN apt-get install -y nodejs
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update && apt-get install yarn
RUN pip3 install awscli --upgrade --user
RUN ~/.local/bin/aws --version
RUN yarn --version
RUN pip3 --version
RUN node --version
